import socket
import asyncio
import os
import random
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

HOST, PORT = '127.0.0.1', 8888

from transitions import Machine

from threading import Thread
import time

import ctypes


class NarcolepticSuperhero(object):

    # Define some states. Most of the time, narcoleptic superheroes are just like
    # everyone else. Except for...
    states = ['asleep', 'hanging out', 'hungry', 'sweaty', 'saving the world']

    def __init__(self, name):

        # No anonymous superheroes on my watch! Every narcoleptic superhero gets
        # a name. Any name at all. SleepyMan. SlumberGirl. You get the idea.
        self.name = name

        # What have we accomplished today?
        self.kittens_rescued = 0

        # Initialize the state machine
        self.machine = Machine(model=self, states=NarcolepticSuperhero.states, initial='asleep')

        # Add some transitions. We could also define these using a static list of
        # dictionaries, as we did with states above, and then pass the list to
        # the Machine initializer as the transitions= argument.

        # At some point, every superhero must rise and shine.
        self.machine.add_transition(trigger='wake_up', source='asleep', dest='hanging out')

        # Superheroes need to keep in shape.
        self.machine.add_transition('work_out', 'hanging out', 'hungry')

        # Those calories won't replenish themselves!
        self.machine.add_transition('eat', 'hungry', 'hanging out')

        # Superheroes are always on call. ALWAYS. But they're not always
        # dressed in work-appropriate clothing.
        self.machine.add_transition('distress_call', '*', 'saving the world',
                                    before='change_into_super_secret_costume')

        # When they get off work, they're all sweaty and disgusting. But before
        # they do anything else, they have to meticulously log their latest
        # escapades. Because the legal department says so.
        self.machine.add_transition('complete_mission', 'saving the world', 'sweaty',
                                    after='update_journal')

        # Sweat is a disorder that can be remedied with water.
        # Unless you've had a particularly long day, in which case... bed time!
        self.machine.add_transition('clean_up', 'sweaty', 'asleep', conditions=['is_exhausted'])
        self.machine.add_transition('clean_up', 'sweaty', 'hanging out')

        # Our NarcolepticSuperhero can fall asleep at pretty much any time.
        self.machine.add_transition('nap', '*', 'asleep')

    def update_journal(self):
        """ Dear Diary, today I saved Mr. Whiskers. Again. """
        self.kittens_rescued += 1

    @property
    def is_exhausted(self):
        """ Basically a coin toss. """
        return random.random() < 0.5

    def change_into_super_secret_costume(self):
        print("Beauty, eh?")


def send_test_message(message: 'Message to send to UDP port 514') -> None:
    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    print('!!!!!!!!!!!! ', (HOST, PORT))
    sock.sendto(message.encode(), (HOST, PORT))


batman = NarcolepticSuperhero("Batman")


def kill():
    ucrtbase = ctypes.CDLL('ucrtbase')
    c_raise = ucrtbase['raise']
    c_raise(signal.SIGINT)


class StateMachine:

    counter = 0

    async def write_messages(loop) -> "Continuously write messages to UDP port 514":
        print("batman.state", batman.state)

        await asyncio.sleep(1)
        send_test_message("test {StateMachine.counter}")
        StateMachine.counter += 1
        if(StateMachine.counter == 3):
            batman.wake_up()
        if(StateMachine.counter == 4):
            Capture.ignore = 1
        elif(StateMachine.counter == 6):
            kill()
            # loop.call_soon_threadsafe(loop.stop)
            # loop.call_soon_threadsafe(loop.close)
        return (await StateMachine.write_messages(loop))


class Capture(asyncio.DatagramProtocol):
    ignore = 0

    def __init__(self):
        super().__init__()

    def connection_made(self, transport) -> "Used by asyncio":
        self.transport = transport

    def datagram_received(self, data, addr) -> "Main entrypoint for processing message":
        # Here is where you would push message to whatever methods/classes you want.
        print(f"Received Syslog message: {data}, ignore {self.ignore}, addr {addr}")


def start_background_loop(loop):
    t = loop.create_datagram_endpoint(Capture, local_addr=(HOST, PORT))
    loop.run_until_complete(t)
    loop.run_until_complete(StateMachine.write_messages(loop))
    loop.run_forever()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    # t = Thread(target=start_background_loop, args=(loop,), daemon=True)
    # t.start()
    # t.join()
    # no threads:
    t = loop.create_datagram_endpoint(Capture, local_addr=(HOST, PORT))
    loop.run_until_complete(t)
    loop.run_until_complete(StateMachine.write_messages(loop))
    # loop.run_forever()
    print("stopped")


try:
    import pytest
    pytestmark = pytest.mark.asyncio
except ImportError:  # pragma: no cover
    pass
