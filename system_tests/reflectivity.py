import numpy as np


class Reflectivity:

    ROWS_NUM = 56
    COLS_NUM = 192

    def __init__(self):
        print(f'using numpy version {np.version.version}')
        self.__data = np.zeros((self.ROWS_NUM, self.COLS_NUM))

    @property
    def data(self) -> None:
        return self.__data

    def clear(self) -> None:
        self.__data = np.zeros((self.ROWS_NUM, self.COLS_NUM))

    def set_pixel(self, row: int, col: int, data: float) -> None:
        if row >= 0 and row < self.ROWS_NUM and col >= 0 and col < self.COLS_NUM:
            self.__data[row][col] = data

    def pixel(self, row: int, col: int) -> float:
        data = -1
        if row >= 0 and row < self.ROWS_NUM and col >= 0 and col < self.COLS_NUM:
            data = self.__data[row][col]
        return data

    def set_row(self, row: int, data: np.ndarray) -> None:
        if row >= 0 and row < self.ROWS_NUM and data.size == self.COLS_NUM:
            self.__data[row] = data

    def row(self, row: int) -> np.ndarray:
        if row >= 0 and row < self.ROWS_NUM:
            data = self.__data[row][:]
        else:
            data = np.zeros(Reflectivity.COLS_NUM)
        return data

    def set_col(self, col: int, data: np.ndarray) -> None:
        if data.size == self.ROWS_NUM and col >= 0 and col < self.COLS_NUM:
            self.__data[:, col] = data

    def col(self, col: int) -> np.ndarray:
        if col >= 0 and col < self.COLS_NUM:
            data = self.__data[:, col]
        else:
            data = np.zeros(Reflectivity.ROWS_NUM)
        return data


# todo:
# to consider to use @setter decriptor
