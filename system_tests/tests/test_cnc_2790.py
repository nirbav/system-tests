import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from cnc2790 import CncOpenBuilds2790Grbl
from unittest import mock
import unittest
import time
import asyncio


class AsyncMock(unittest.mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)


class AsyncMockWait(unittest.mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        await asyncio.sleep(0.4)
        return super().__call__(*args, **kwargs)


class TestCncOpenBuilds2790Grbl(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.cnc_2790_grbl = CncOpenBuilds2790Grbl()

    @classmethod
    def tearDownClass(cls):
        pass

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.home', new_callable=AsyncMock)
    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.cancel_alarm', new_callable=AsyncMock)
    @mock.patch('serial.Serial.isOpen', return_value=True)
    @mock.patch('asyncio.sleep', new_callable=AsyncMock)
    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.open_serial', new_callable=AsyncMock)
    def test_init(self, open_serial_mock, time_mock, ser_is_open_mock, cancel_alarm_mock, home_mock):
        asyncio.run(self.cnc_2790_grbl.init())
        open_serial_mock.assert_called_once_with()
        time_mock.assert_called_once_with(0.1)
        ser_is_open_mock.assert_called_once_with()
        cancel_alarm_mock.assert_called_once_with()
        home_mock.assert_called_once_with()
        self.assertEqual(self.cnc_2790_grbl.ser.baudrate, 115200)
        self.assertEqual(self.cnc_2790_grbl.ser.port, 'COM4')
        self.assertEqual(self.cnc_2790_grbl.ser.timeout, None)
        self.assertEqual(self.cnc_2790_grbl.jog, 5)
        self.assertEqual(self.cnc_2790_grbl.feed_rate, 4997)

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.home', new_callable=AsyncMock)
    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.cancel_alarm', new_callable=AsyncMock)
    @mock.patch('serial.Serial.isOpen', return_value=False)
    @mock.patch('asyncio.sleep', new_callable=AsyncMock)
    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.open_serial', new_callable=AsyncMock)
    def test_init_serial_closed(self, open_serial_mock, time_mock, ser_is_open_mock, cancel_alarm_mock, home_mock):
        asyncio.run(self.cnc_2790_grbl.init())
        open_serial_mock.assert_called_once_with()
        time_mock.assert_called_once_with(0.1)
        ser_is_open_mock.assert_called_once_with()
        cancel_alarm_mock.assert_not_called()
        home_mock.assert_not_called()

    @mock.patch('serial.Serial.close', new_callable=AsyncMock)
    def test_stop(self, close_serial_mock):
        asyncio.run(self.cnc_2790_grbl.stop())
        close_serial_mock.assert_called_once_with()

    @mock.patch('serial.Serial.open', new_callable=AsyncMock)
    @mock.patch('serial.Serial.isOpen', return_value=False)
    def test_open_serial(self, ser_is_open_mock, ser_open_mock):
        asyncio.run(self.cnc_2790_grbl.open_serial())
        ser_is_open_mock.assert_called_once_with()
        ser_open_mock.assert_called_once_with()

    @mock.patch('serial.Serial.open')
    @mock.patch('serial.Serial.isOpen', return_value=True)
    def test_open_serial_port_open(self, ser_is_open_mock, ser_open_mock):
        asyncio.run(self.cnc_2790_grbl.open_serial())
        ser_is_open_mock.assert_called_once_with()
        ser_open_mock.assert_not_called()

    @mock.patch('serial.Serial.open', side_effect=IOError('test'))
    @mock.patch('serial.Serial.isOpen', return_value=False)
    def test_open_serial_exception(self, ser_is_open_mock, ser_open_mock):
        with self.assertRaises(IOError):
            asyncio.run(self.cnc_2790_grbl.open_serial())

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock)
    def test_cancel_alarm(self, write_and_wait_mock):
        asyncio.run(self.cnc_2790_grbl.cancel_alarm())
        write_and_wait_mock.assert_called_once_with('$')

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock)
    def test_home(self, write_and_wait_mock):
        asyncio.run(self.cnc_2790_grbl.home())
        write_and_wait_mock.assert_called_once_with('$H')

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock)
    def test_up(self, write_and_wait_mock):
        asyncio.run(self.cnc_2790_grbl.up())
        write_and_wait_mock.assert_called_once_with('$J=G91G21Y5F4997')

    @ mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock)
    def test_down(self, write_and_wait_mock):
        asyncio.run(self.cnc_2790_grbl.down())
        write_and_wait_mock.assert_called_once_with('$J=G91G21Y-5F4997')

    @ mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock)
    def test_right(self, write_and_wait_mock):
        asyncio.run(self.cnc_2790_grbl.right())
        write_and_wait_mock.assert_called_once_with('$J=G91G21X5F4997')

    @ mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock)
    def test_left(self, write_and_wait_mock):
        asyncio.run(self.cnc_2790_grbl.left())
        write_and_wait_mock.assert_called_once_with('$J=G91G21X-5F4997')

    @mock.patch('serial.Serial.write')
    @mock.patch('serial.Serial.isOpen', return_value=True)
    @mock.patch('serial.Serial.flushInput')
    def test_write(self, flush_input_mock, ser_is_open_mock, ser_write_mock):
        self.cnc_2790_grbl.write('test')
        ser_is_open_mock.assert_called_once_with()
        ser_write_mock.assert_called_once_with('test\n'.encode())

    @mock.patch('serial.Serial.readline')
    @mock.patch('serial.Serial.write', new_callable=AsyncMock)
    @mock.patch('serial.Serial.flushInput', new_callable=AsyncMock)
    @mock.patch('serial.Serial.isOpen', return_value=True)
    def test_write_and_wait(self, ser_is_open_mock, flush_input_mock, write_mock, readline_mock):
        resp = asyncio.run(self.cnc_2790_grbl.write_and_wait('test'))
        ser_is_open_mock.assert_called_once_with()
        self.assertEqual(self.cnc_2790_grbl.ser.timeout, None)
        write_mock.assert_called_once_with(b'test\n')
        readline_mock.assert_called_once_with()

    @mock.patch('serial.Serial.write')
    @mock.patch('serial.Serial.isOpen', return_value=False)
    def test_write_serial_closed(self, ser_is_open_mock, ser_write_mock):
        self.cnc_2790_grbl.write('test')
        ser_is_open_mock.assert_called_once_with()
        ser_write_mock.assert_not_called()

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMockWait)
    def test_goto_wait_for_complete_blocking(self, check_for_completion_mock):
        timestamp = time.perf_counter()
        asyncio.run(self.cnc_2790_grbl.goto(5, 0))
        elapsed = time.perf_counter() - timestamp
        print('test_goto_wait_for_complete executed in {elapsed:0.2f} seconds')
        self.assertGreater(elapsed, 0.3)

    @mock.patch('cnc2790.CncOpenBuilds2790Grbl.write_and_wait', new_callable=AsyncMock, return_value=b'<Idle|MPos:-733.000,-2322.984,0.000|FS:0,0>\r\n')
    def test_where(self, write_and_wait_mock):
        x, y = asyncio.run(self.cnc_2790_grbl.where())
        self.assertEqual(x, -733)
        self.assertEqual(y, -2322.984)


if __name__ == '__main__':
    asyncio.run(unittest.main())
