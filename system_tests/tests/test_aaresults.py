import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from anac.results import AngularAccuracyResults
from scan import Scan
import pandas as pd
import numpy as np
import pathlib
from unittest.mock import patch, mock_open
import unittest


FIRST_COL = 3


class TestAngularAccuracyResults(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.tested_filename = os.path.join('./', 'tested_angular_accuracy_results.csv')
        cls.results = AngularAccuracyResults(cls.tested_filename)
        cls.empty_file = pd.read_csv(cls.tested_filename)

    @ classmethod
    def tearDownClass(cls):
        os.remove(cls.tested_filename)

    def test_create_new_file(self):
        tmp_filename = self.tested_filename + '.tmp'
        AngularAccuracyResults(tmp_filename)
        try:
            content = pd.read_csv(tmp_filename)
        except:
            self.assertTrue(False)
        os.remove(tmp_filename)
        self.assertTrue(content.equals(self.empty_file))

    def test_set_pixel(self):
        self.results.set_refs(0, 0, Scan.CENTER, [8])
        self.assertEqual(self.results.results.iat[((0 + 1) * (0 + 1)) - 1, FIRST_COL], 8)
        self.results.set_refs(55, 191, Scan.CENTER, [-8])
        self.assertEqual(self.results.results.iat[((55 + 1) * (191 + 1)) - 1, FIRST_COL], -8)

    def test_set_pixel_wrong_list(self):
        with self.assertRaises(Exception) as context:
            self.results.set_refs(0, 0, Scan.CENTER, [8, 2])
        with self.assertRaises(Exception) as context:
            self.results.set_refs(0, 0, Scan.CENTER, [])

    def test_set_pixel_wrong_pixel(self):
        with self.assertRaises(Exception) as context:
            self.results.set_refs(56, 0, Scan.CENTER, [0])
        with self.assertRaises(Exception) as context:
            self.results.set_refs(0, 192, Scan.CENTER, [0])
        with self.assertRaises(Exception) as context:
            self.results.set_refs(0, -2, Scan.CENTER, [0])

    def test_set_pixel_wrong_scan(self):
        with self.assertRaises(Exception) as context:
            self.results.set_refs(0, 0, len(Scan) + 1, [0])
        with self.assertRaises(Exception) as context:
            self.results.set_refs(0, 0, 0, [0])

    def test_set_col(self):
        tested_values = np.arange(0, 56, 1).tolist()
        tested_col = 191
        self.results.set_refs(-1, tested_col, Scan.CENTER, tested_values)
        res = self.results.results.iloc[np.arange(tested_col, tested_col + 10752, 192).tolist(),
                                        Scan.CENTER + self.results.FIRST_COL]
        self.assertEqual(res.tolist(), tested_values)

    def test_set_row(self):
        tested_values = np.arange(0, 192, 1).tolist()
        tested_row = 0
        self.results.set_refs(tested_row, -1, Scan.CENTER, tested_values)
        self.results.results.to_csv(self.tested_filename, index=False)
        res = self.results.results.iloc[tested_row * 192:tested_row * 192 + 192,
                                        Scan.CENTER + self.results.FIRST_COL]
        self.assertEqual(res.tolist(), tested_values)

    def test_max_pixel(self):
        row = col = 0
        self.results.results.iat[0, Scan.CENTER + self.results.FIRST_COL] = 11
        self.results.results.iat[0, Scan.RIGHT_DOWN + self.results.FIRST_COL] = 22
        self.results.results.iat[0, Scan.UP + self.results.FIRST_COL] = 33
        self.assertEqual(self.results.max_refs(0, 0), 33)

    def test_to_csv(self):
        self.results.results.iloc[0:10752, 3:13] = 666
        self.results.to_csv()
        expected = pd.read_csv(self.tested_filename)
        self.assertTrue(expected.equals(self.results.results))


if __name__ == '__main__':
    unittest.main()
