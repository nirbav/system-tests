import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from anac.fsm import AngularAccuracyFsm
from capture import Reflectivity
from capture import Capture
from cnc2790 import CncOpenBuilds2790Grbl
from board import BoardController
from alignment import TrxBoardAlignment
from lidar import Lidar
from anac.results import AngularAccuracyResults
from scan import Scan
from unittest import mock
import numpy as np
import unittest
import time
import asyncio


class AsyncMock(unittest.mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)


class AsyncMockWait(unittest.mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        await asyncio.sleep(0.4)
        return super().__call__(*args, **kwargs)


class TestAngularAccuracyFsm(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        loop = asyncio.get_event_loop()
        reflectivity = Reflectivity()
        cls.capture = Capture(reflectivity)
        cls.board_controller = BoardController(CncOpenBuilds2790Grbl())
        cls.trx_board_align = TrxBoardAlignment(os.path.join('./', 'tested_y0_mipi_1.csv'))
        cls.lidar_controller = Lidar(1)
        cls.results = AngularAccuracyResults(os.path.join(
            './', 'tested_angular_accuracy_results.csv'))
        cls.tested_aafsm = AngularAccuracyFsm(loop,
                                              cls.capture,
                                              cls.board_controller,
                                              cls.trx_board_align,
                                              cls.lidar_controller,
                                              cls.results)

    @ classmethod
    def tearDownClass(cls):
        pass

    @ mock.patch('anac.fsm.AngularAccuracyFsm.do_test', new_callable=AsyncMock)
    @ mock.patch('board.BoardController.init', new_callable=AsyncMock)
    @ mock.patch('lidar.Lidar.play', new_callable=AsyncMock)
    @ mock.patch('lidar.Lidar.factory', new_callable=AsyncMock, return_value=0xBB)
    def test_start_state(self, factory_mock, play_mock, board_mock, test_state_mock):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.tested_aafsm.start())
        factory_mock.assert_called_once_with()
        play_mock.assert_called_once_with()
        board_mock.assert_called_once_with()
        self.assertEqual(self.tested_aafsm.factory, 0xBB)

    @ mock.patch('anac.fsm.AngularAccuracyFsm.analyze', new_callable=AsyncMock)
    @ mock.patch('anac.fsm.AngularAccuracyFsm.scan_col', new_callable=AsyncMock)
    @ mock.patch('board.BoardController.where', new_callable=AsyncMock, return_value=[-1, 0])
    def test_do_test_state(self, where_mock, scan_mock, analyze_state_mock):
        self.tested_aafsm.factory = 0xBB
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.tested_aafsm.do_test())
        where_mock.assert_called_once_with()
        x0, y0 = where_mock.return_value
        self.assertEqual(len(scan_mock.mock_calls), 192)

    @ mock.patch('anac.fsm.AngularAccuracyFsm._record')
    @ mock.patch('anac.fsm.AngularAccuracyFsm._capture', new_callable=AsyncMock)
    @ mock.patch('board.BoardController.goto', new_callable=AsyncMock)
    @ mock.patch('alignment.TrxBoardAlignment.y0', new_callable=AsyncMock, return_calue=1)
    def test_scan_col(self, align_mock, goto_mock, capture_mock, record_mock):
        col = 1
        x0 = 0
        self.tested_aafsm.factory = 0xBB
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.tested_aafsm.scan_col(col, x0))
        align_mock.assert_called_once_with(col)
        y = align_mock.return_value
        goto_mock.assert_called_once_with(x0, y)
        scans = [Scan.CENTER]
        for scan in scans:
            capture_mock.assert_called_once_with()
            record_mock.assert_called_once_with(col, scan)

    @ mock.patch('capture.Capture.stop')
    @ mock.patch('asyncio.sleep', new_callable=AsyncMock)
    @ mock.patch('capture.Capture.start')
    def test__capture(self, start_mock, sleep_mock, stop_mock):
        self.tested_aafsm.factory = 0xBB
        order = mock.MagicMock()
        order.attach_mock(start_mock, 'the_start_mock')
        order.attach_mock(sleep_mock, 'the_sleep_mock')
        order.attach_mock(stop_mock, 'the_stop_mock')
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.tested_aafsm._capture())
        expected_calls = [mock.call.the_start_mock(
            0xBB, 1), mock.call.the_sleep_mock(0.1), mock.call.the_stop_mock()]
        self.assertEqual(order.mock_calls, expected_calls)

    @ mock.patch('anac.results.AngularAccuracyResults.set_refs')
    def test__record_args(self, ref_mock):
        col = 0
        scan_na = -9
        self.tested_aafsm._record(col, scan_na)
        ref_mock.assert_called_once_with(-1, col, scan_na, mock.ANY)

    def test__record_refs(self):
        col = 0
        scan = 1
        expected_refs = np.arange(0, 112, 2)
        self.capture.reflectivity.data[:, col] = expected_refs
        self.tested_aafsm._record(col, scan)
        tested_refs = self.tested_aafsm.results.results.iloc[np.arange(
            col, col + 10752, 192).tolist(), scan + 2]
        self.assertTrue(np.array_equal(tested_refs, expected_refs))

    @ mock.patch('anac.fsm.AngularAccuracyFsm.close')
    @ mock.patch('anac.results.AngularAccuracyResults.to_csv')
    @ mock.patch('board.BoardController.stop', new_callable=AsyncMock)
    @ mock.patch('lidar.Lidar.stop', new_callable=AsyncMock)
    def test_analyze_state(self, lidar_stop_mock, board_stop_mock, to_csv_mock, close_state_mock):
        order = mock.MagicMock()
        order.attach_mock(close_state_mock, 'the_close_state_mock')
        order.attach_mock(to_csv_mock, 'the_to_csv_mock')
        order.attach_mock(board_stop_mock, 'the_board_stop_mock')
        order.attach_mock(lidar_stop_mock, 'the_lidar_stop_mock')
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.tested_aafsm.analyze())
        expected_calls = [mock.call.the_lidar_stop_mock(), mock.call.the_board_stop_mock(),
                          mock.call.the_to_csv_mock(index=False), mock.call.the_close_state_mock()]
        self.assertEqual(order.mock_calls, expected_calls)

    @ mock.patch('anac.fsm.AngularAccuracyFsm.close')
    @ mock.patch('board.BoardController.stop', new_callable=AsyncMock)
    @ mock.patch('anac.results.AngularAccuracyResults.to_csv')
    @ mock.patch('lidar.Lidar.stop', new_callable=AsyncMock)
    def test_close_state(self, lidar_stop_mock, to_csv_mock, board_stop_mock, close_state_mock):
        self.tested_aafsm.close()


if __name__ == '__main__':
    unittest.main()

# todo:
# to find a way to enfource order
