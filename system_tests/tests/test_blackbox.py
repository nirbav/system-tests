import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from anac.fsm import AngularAccuracyFsm
from capture import Reflectivity
from capture import Capture
from cnc2790 import CncOpenBuilds2790Grbl
from board import BoardController
from alignment import TrxBoardAlignment
from lidar import Lidar
from sender import Sender
from anac.results import AngularAccuracyResults
from unittest import mock
import unittest
import time
import asyncio
import signal


signal.signal(signal.SIGINT, signal.SIG_DFL)
HOST, PORT = '127.0.0.1', 8888


class AsyncMock(unittest.mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)


class AsyncMockWait(unittest.mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        await asyncio.sleep(0.4)
        return super().__call__(*args, **kwargs)


class TestAngularAccuracyFsm(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        reflectivity = Reflectivity()
        cls.capture = Capture(reflectivity)
        cls.board_controller = BoardController(CncOpenBuilds2790Grbl())
        cls.trx_board_align = TrxBoardAlignment(os.path.join('./', 'tested_y0_mipi_1.csv'))
        cls.lidar_controller = Lidar(1)
        cls.results = AngularAccuracyResults(os.path.join(
            './', 'tested_angular_accuracy_results.csv'))

    @ classmethod
    def tearDownClass(cls):
        pass

    def notest_run_aast_rcv_2_packets_and_suiside(self):
        gap = 1
        num_of_iter = 20
        num_of_datagrams = 20
        self.__start_sender_daemon(num_of_datagrams, gap)
        loop = asyncio.get_event_loop()
        fsm = AngularAccuracyFsm(loop,
                                 self.capture,
                                 self.board_controller,
                                 self.trx_board_align,
                                 self.lidar_controller,
                                 self.results)
        loop.run_until_complete(loop.create_datagram_endpoint(
            lambda: self.capture, local_addr=(HOST, PORT)))
        loop.run_until_complete(fsm.listen_for_tests(num_of_iter, gap),)
        loop.run_forever()
        print('stopped')

    def __start_sender_daemon(self, num_of_datagrams, gap):
        pcap = os.path.join('./', 'tested.pcap')
        s = Sender(HOST, PORT, pcap, gap)
        s.run(num_of_datagrams)


if __name__ == '__main__':
    asyncio.run(unittest.main())
