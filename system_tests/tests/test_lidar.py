
import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from lidar import Lidar
from unittest import mock
import unittest
import requests
import responses

HOST, PORT = '172.23.37.11', 3000
API_URL = 'http://{}:{}/Api/ExecuteIpcApi'.format(HOST, PORT)
GOOD_RESP_JSON = {'resp': {'req': {'name': 'set_play_stop', 'data': True},
                           'returnCode': 'ok', 'returnCodeData': {}, 'returnCodeError': None}}
REGS_URL = 'http://{}:{}/api/v2/registers/hsf/'.format(HOST, PORT)
FACTORY_URL = REGS_URL + '{}/parameter?registerGroupName=Saturn2&parameterName=HSF_DMA_{}_PREFIX'
STATUS_OK = 200
STATUS_NOT_FOUND = 404


class TestLidar(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.tested_trx_mipi = 1
        cls.lidar = Lidar(cls.tested_trx_mipi)

    @classmethod
    def tearDownClass(cls):
        pass

    @responses.activate
    def test_play(self):
        responses.add(responses.POST, API_URL, json=GOOD_RESP_JSON, status=STATUS_OK)
        self.lidar.play()
        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(responses.calls[0].request.url, API_URL)
        self.assertEqual(responses.calls[0].response.status_code, STATUS_OK)

    @mock.patch('requests.post')
    def test_play_cmd(self, requests_mock):
        self.lidar.play()
        requests_mock.assert_called_once_with(
            API_URL, json={'cmdType': 'set_play_stop', 'data': True})

    @responses.activate
    def test_stop(self):
        responses.add(responses.POST, API_URL, json=GOOD_RESP_JSON, status=STATUS_OK)
        self.lidar.stop()
        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(responses.calls[0].request.url, API_URL)
        self.assertEqual(responses.calls[0].response.status_code, STATUS_OK)

    @mock.patch('requests.post')
    def test_stop_cmd(self, requests_mock):
        self.lidar.stop()
        requests_mock.assert_called_once_with(
            API_URL, json={'cmdType': 'set_play_stop', 'data': False})

    @responses.activate
    def test_request_factory_ok(self):
        tested_hsf = 1
        url = FACTORY_URL.format(tested_hsf, (self.tested_trx_mipi - 1) % 4)
        responses.add(responses.GET, url, json={'value': 0xBB}, status=STATUS_OK)
        factory = self.lidar.factory()
        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(responses.calls[0].request.url, url)
        self.assertEqual(responses.calls[0].response.status_code, STATUS_OK)
        self.assertEqual(factory, 0xBB)

    @responses.activate
    def test_request_factory_error(self):
        tested_hsf = 1
        url = FACTORY_URL.format(tested_hsf, (self.tested_trx_mipi - 1) % 4)
        responses.add(responses.GET, url, status=STATUS_NOT_FOUND)
        factory = self.lidar.factory()
        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(responses.calls[0].request.url, url)
        self.assertEqual(responses.calls[0].response.status_code, STATUS_NOT_FOUND)
        self.assertEqual(factory, 0)

    @mock.patch('requests.get')
    def test_request_factory_cmd_2nd_hsf(self, requests_mock):
        tested_trx_mipi = 6
        self.lidar.trx_mipi = tested_trx_mipi
        tested_hsf = 2
        url = FACTORY_URL.format(tested_hsf, (tested_trx_mipi - 1) % 4)
        factory = self.lidar.factory()
        self.lidar.trx_mipi = self.tested_trx_mipi
        requests_mock.assert_called_once_with(url)


if __name__ == '__main__':
    unittest.main()
