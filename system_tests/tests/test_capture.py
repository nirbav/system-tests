
import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from capture import Capture
from reflectivity import Reflectivity
from unittest import mock
import unittest
import dpkt
import numpy as np
import pathlib

HOST, PORT = '127.0.0.1', 8888
FACTORY = 0xBB
IMAGE_DONE_MASK = 0x80


class TestCapture(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.addr = (HOST, PORT)
        reflectivity = Reflectivity()
        cls.capture = Capture(reflectivity)
        header = [FACTORY] + [0] + [0xFF] * 6
        image_done_header = [FACTORY] + [IMAGE_DONE_MASK] + [0xFF] * 6
        wrong_factory_header = [0xAA] + [0xFF] * 7
        dummy_points_data = [0xAB] * 1024
        for p in range(128):
            dummy_points_data[p * 8] = row = 0
            dummy_points_data[1 + p * 8] = col = p
        cls.dummy_datagram = bytearray(header) + bytearray(dummy_points_data)
        cls.dummy_wrong_factory_datagram = bytearray(
            wrong_factory_header) + bytearray(dummy_points_data)
        cls.dummy_image_done_datagram = bytearray(image_done_header) + bytearray(dummy_points_data)
        cls.dummy_partial_image_done_datagram = cls.dummy_image_done_datagram

    def setUp(self):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def tearDown(self):
        pass

    def test_datagram_received_row_0_first_128_points(self):
        self.capture.start(FACTORY)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_datagram, self.addr)
        self.assertTrue(np.all(self.capture.reflectivity.data[0, 0:128] == 0xAB))
        self.assertTrue(np.all(self.capture.reflectivity.data[0, 129:192] == 0))
        self.assertFalse(self.capture.done())

    def test_datagram_ignore_on_init(self):
        self.capture.stop()
        self.capture.datagram_received(self.dummy_datagram, self.addr)
        self.assertTrue(self.capture.paused())
        self.assertTrue(np.all(self.capture.reflectivity.data[0, ] == 0))
        self.assertFalse(self.capture.done())

    def test_datagram_received(self):
        self.capture.start(FACTORY)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_datagram, self.addr)
        self.assertTrue(np.all(self.capture.reflectivity.data[0, 0:128] == 0xAB))
        self.assertTrue(np.all(self.capture.reflectivity.data[0, 129:192] == 0))

    def test_datagram_received_ignore_different_factory(self):
        self.capture.start(FACTORY)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_wrong_factory_datagram, self.addr)
        self.assertEqual(self.capture.image_done_counter, 0)

    def test_one_image_done_required(self):
        self.capture.start(FACTORY)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_image_done_datagram, self.addr)
        self.assertTrue(self.capture.done())
        self.assertEqual(self.capture.reflectivity.data[0][0], 0xAB)

    def test_two_image_dones_required(self):
        self.capture.start(FACTORY, 2)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_image_done_datagram, self.addr)
        self.assertFalse(self.capture.done())
        self.capture.datagram_received(self.dummy_image_done_datagram, self.addr)
        self.assertTrue(self.capture.done())
        self.assertEqual(self.capture.reflectivity.data[0][0], 0xAB * 2)

    def test_start(self):
        self.capture.start(FACTORY)
        self.assertFalse(self.capture.paused())
        self.assertTrue(np.all(self.capture.reflectivity.data == 0))
        self.assertFalse(self.capture.done())

    def test_start_without_stop(self):
        self.capture.start(FACTORY)
        self.assertFalse(self.capture.paused())
        self.assertTrue(np.all(self.capture.reflectivity.data == 0))
        self.capture.datagram_received(self.dummy_datagram, self.addr)
        self.assertFalse(self.capture.done())
        self.capture.start(FACTORY)
        self.assertFalse(self.capture.paused())
        self.assertTrue(np.all(self.capture.reflectivity.data == 0))

    def test_stop(self):
        self.capture.start(FACTORY)
        self.assertFalse(self.capture.paused())
        self.capture.stop()
        self.assertTrue(self.capture.paused())

    def test_stop_without_start(self):
        self.capture.start(FACTORY)
        self.assertFalse(self.capture.paused())
        self.capture.stop()
        self.assertTrue(self.capture.paused())
        self.capture.stop()
        self.assertTrue(self.capture.paused())

    def test_done(self):
        self.capture.start(FACTORY)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_image_done_datagram, self.addr)
        self.assertTrue(self.capture.done())

    def test_paused(self):
        self.capture.start(FACTORY)
        self.capture.datagram_received(self.dummy_partial_image_done_datagram, self.addr)
        self.capture.datagram_received(self.dummy_image_done_datagram, self.addr)
        self.assertTrue(self.capture.paused())


if __name__ == '__main__':
    unittest.main()


# todo:
# shorter dummy packets creation
# move the pcap sender to seperate class
