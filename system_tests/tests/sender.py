
import os
import sys
import dpkt
import socket
import time
import threading


class Sender():

    def __init__(self, host, port, pcap_filename, gap):
        self.host = host
        self.port = port
        self.addr = (host, port)
        self.datagrams = []
        self.pcap_filename = pcap_filename
        self.__read_pcap()
        self.gap = gap

    def __prepare_datagrams(self, pcap):
        for timestamp, buf in pcap:
            str(timestamp)
            eth = dpkt.ethernet.Ethernet(buf)
            if not isinstance(eth.data, dpkt.ip.IP):
                continue
            ip = eth.data
            if isinstance(ip.data, dpkt.udp.UDP):
                udp = ip.data
                if udp.dport != self.port:
                    continue
                self.datagrams.append(udp.data)

    def __read_pcap(self):
        with open(self.pcap_filename, "rb") as f:
            r = dpkt.pcap.Reader(f)
            print(r)
            self.__prepare_datagrams(r)

    def send_datagram(self, num_of_datagrams=0):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        counter = forever = 0
        if num_of_datagrams == forever:
            print(f'going to send packests in forever loop')
        else:
            print(f'going to send {num_of_datagrams} packests')
        while(True):
            counter += 1
            datagram = self.datagrams.pop(0)
            self.datagrams.append(datagram)
            sock.sendto(datagram, self.addr)
            time.sleep(self.gap)
            if counter == num_of_datagrams and forever != num_of_datagrams:
                break
        print(f'{num_of_datagrams} packests were sent')
        sock.close()

    def run(self, num_of_datagrams=1):
        t = threading.Thread(target=self.send_datagram, args=(num_of_datagrams,))
        t.start()
