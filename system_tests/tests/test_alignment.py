import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from alignment import TrxBoardAlignment
import pandas as pd
import pathlib
from unittest.mock import patch, mock_open
import unittest


class TestTrxBoardAlignment(unittest.TestCase):

    @ classmethod
    def setUpClass(cls):
        tested_trx_mipi = 1
        cls.tested_y0_filename = os.path.join(
            './', 'tested_y0_mipi_{}.csv'.format(tested_trx_mipi))
        cls.trx_board_align = TrxBoardAlignment(cls.tested_y0_filename)
        cls.tested_cy0 = pd.read_csv(cls.tested_y0_filename)

    @ classmethod
    def tearDownClass(cls):
        pass

    def test_init(self):
        self.assertEqual(self.trx_board_align.y0_filename, self.tested_y0_filename)

    def test_read_ok(self):
        self.assertTrue(self.trx_board_align.cy0.equals(self.tested_cy0))

    def test_x_axis_trx_board_align(self):
        self.assertEqual(-1, self.trx_board_align.x0(2))

    def test_y_axis_trx_board_align(self):
        tested_col = 3
        y = self.trx_board_align.y0(tested_col)
        self.assertEqual(30, y)
        tested_col = 192
        y = self.trx_board_align.y0(tested_col)
        self.assertEqual(-1, y)

    def test_x_axis_trx_board_align(self):
        self.assertEqual(0, self.trx_board_align.z0(4))


if __name__ == '__main__':
    unittest.main()
