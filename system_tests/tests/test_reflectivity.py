
import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from reflectivity import Reflectivity
from unittest import mock
import unittest
import numpy as np


class TestBoardController(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.reflectivity = Reflectivity()

    @ classmethod
    def tearDownClass(cls):
        pass

    def beforeEach(self):
        self.reflectivity.clear()

    def test_init(self):
        self.assertTrue(np.all(self.reflectivity.data) == 0)

    def test_clear(self):
        self.assertTrue(np.all(self.reflectivity.data) == 0)

    def test_pixel(self):
        self.reflectivity.set_pixel(0, 0, 1.0)
        self.assertEqual(self.reflectivity.pixel(0, 0), 1.0)
        try:
            self.reflectivity.set_pixel(-1, 0, 2.0)
            self.reflectivity.set_pixel(55 + 1, 0, 2.0)
            self.reflectivity.set_pixel(0, -1, 2.0)
            self.reflectivity.set_pixel(0, 191 + 1, 2.0)
        except Exception:
            self.assertTrue(False)

    def test_row(self):
        data = np.ones(Reflectivity.COLS_NUM)
        self.reflectivity.set_row(0, data)
        self.assertTrue((self.reflectivity.row(0) == data).all())
        try:
            self.reflectivity.set_row(-1, data)
            self.reflectivity.set_row(55 + 1, data)
        except Exception:
            self.assertTrue(False)
        almost = np.ones(Reflectivity.COLS_NUM - 1)
        self.reflectivity.clear()
        self.reflectivity.set_row(1, almost)
        self.assertTrue((self.reflectivity.row(1) == np.zeros(Reflectivity.COLS_NUM)).all())
        more = np.ones(Reflectivity.COLS_NUM + 1)
        self.assertTrue((self.reflectivity.row(1) == np.zeros(Reflectivity.COLS_NUM)).all())

    def test_col(self):
        data = np.ones(Reflectivity.ROWS_NUM)
        self.reflectivity.set_col(0, data)
        self.assertTrue((self.reflectivity.col(0) == data).all())
        try:
            self.reflectivity.set_col(-1, data)
            self.reflectivity.set_col(111 + 1, data)
        except Exception:
            self.assertTrue(False)
        self.reflectivity.clear()
        almost = np.ones(Reflectivity.ROWS_NUM - 1)
        self.reflectivity.set_col(1, almost)
        self.assertTrue((self.reflectivity.col(1) == np.zeros(Reflectivity.ROWS_NUM)).all())
        more = np.ones(Reflectivity.ROWS_NUM + 1)
        self.assertTrue((self.reflectivity.col(1) == np.zeros(Reflectivity.ROWS_NUM)).all())


if __name__ == '__main__':
    unittest.main()
