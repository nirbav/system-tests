from enum import IntEnum
import os


class Scan(IntEnum):
    CENTER = 1
    RIGHT = 2
    RIGHT_DOWN = 3
    DOWN = 4
    LEFT_DOWN = 5
    LEFT = 6
    LEFT_UP = 7
    UP = 8
    RIGHT_UP = 9


class ScansOrder():
    def __init__(self):
        self.scan = Scan()

    def scan_valid():
        pass
