import os
import csv
import pathlib
import pandas as pd
import asyncio


class TrxBoardAlignment:
    """
        map between trx pixels row column to cnc board coordinates
        taken from csv, per trx mipi port
    """

    def __init__(self, y0_filename) -> None:
        self.y0_filename = y0_filename
        self.cy0 = pd.read_csv(self.y0_filename)
        assert(len(self.cy0['y']) == 192)

    def x0(self, col) -> int:
        return -1

    def y0(self, col) -> int:
        c = -1
        if col >= 0 and col < 192:
            c = self.cy0['y'][col]
        return c

    def z0(self, col) -> int:
        return 0
