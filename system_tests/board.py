from cnc2790 import CncOpenBuilds2790Grbl
import asyncio


class BoardController:

    def __init__(self, controller: CncOpenBuilds2790Grbl) -> None:
        self.controller = CncOpenBuilds2790Grbl()

    async def init(self) -> None:
        await self.controller.init()

    async def stop(self) -> None:
        await self.controller.stop()

    async def home(self) -> None:
        await self.controller.home()

    async def up(self) -> None:
        await self.controller.up()

    async def down(self) -> None:
        await self.controller.down()

    async def right(self) -> None:
        await self.controller.right()

    async def left(self) -> None:
        await self.controller.left()

    async def goto(self, x: float = 0, y: float = 0) -> None:
        await self.controller.goto(x, y)

    async def where(self) -> [float, float]:
        return await self.controller.where()
