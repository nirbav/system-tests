import requests


HOST, PORT = '172.23.37.11', 3000
ipc_url = 'http://{}:{}/Api/ExecuteIpcApi'.format(HOST, PORT)
registers_url = 'http://{}:{}/api/v2/registers'.format(HOST, PORT)
hsf_parmeter_rest = '/hsf/{}/parameter?registerGroupName=Saturn2&parameterName={}'

OK = 200


class Lidar():

    def __init__(self, trx_mipi):
        self.trx_mipi = trx_mipi

    def play(self):
        data = {'cmdType': 'set_play_stop', 'data': True}
        resp = requests.post(ipc_url, json=data)
        if resp.status_code != OK:
            print(f'http post {ipc_url}/{data} failed with {resp.status_code}')

    def stop(self):
        data = {'cmdType': 'set_play_stop', 'data': False}
        resp = requests.post(ipc_url, json=data)
        if resp.status_code != OK:
            print(f'http post {ipc_url}/{data} failed with {resp.status_code}')

    def factory(self):
        factory = 0
        hsf_index = self.hsf_index_by_mipi(self.trx_mipi)
        parameter_name = 'HSF_DMA_{}_PREFIX'.format((self.trx_mipi - 1) % 4)
        url = registers_url + hsf_parmeter_rest.format(hsf_index, parameter_name)
        resp = requests.get(url)
        if resp.status_code != OK:
            print(f'http get {url} failed with {resp.status_code}')
        else:
            factory = resp.json()['value']
        return factory

    def hsf_index_by_mipi(self, trx_mipi):
        hsf_index = 1
        if trx_mipi in [5, 6, 7, 8]:
            hsf_index = 2
        return hsf_index
