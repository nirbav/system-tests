import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from enum import IntEnum
from scan import Scan
import os
import csv
import pathlib
import pandas as pd
import numpy as np


class AngularAccuracyResults:
    """
        hold the angular accuracy system test results
        that is list of the scanned reflectivities for each pixel
        not scanned will be equals to -1
    """

    FIRST_COL = 2

    def __init__(self, filename) -> None:
        self.filename = filename
        self.__create_empty_file()
        self.results = pd.read_csv(self.filename)

    def set_refs(self, row: int, col: int, scan: Scan, data: list) -> None:
        assert (row >= 0 and row < 56 or row == -1)
        assert (col >= 0 and col < 192 or col == -1)
        assert (scan > 0 and scan <= len(Scan))
        if row != -1 and col != -1:
            self.__set_pixel_ref(row, col, scan, data)
        elif row != -1 and col == -1:
            self.__set_row_refs(row, scan, data)
        elif row == -1 and col != -1:
            self.__set_col_refs(col, scan, data)
        else:
            assert (False)

    def max_refs(self, row: int, col: int) -> list:
        assert (row >= 0 and row < 56 or row == -1)
        assert (col >= 0 and col < 192 or col == -1)
        if row != -1 and col != -1:
            return self.__max_pixel_ref(row, col)
        elif row != -1 and col == -1:
            assert (False)
            return self.__max_row_refs(row)
        elif row == -1 and col != -1:
            assert (False)
            return self.__max_col_refs(col)
        else:
            assert (False)

    def __set_col_refs(self, col: int, scan: Scan, data: list) -> None:
        assert (len(data) == 56)
        self.results.iloc[np.arange(col, col + 10752, 192).tolist(),
                          int(scan) + self.FIRST_COL] = data

    def __set_row_refs(self, row: int, scan: Scan, data: list) -> None:
        assert (len(data) == 192)
        self.results.iloc[row * 192:row * 192 + 192, int(scan) + self.FIRST_COL] = data

    def __set_pixel_ref(self, row: int, col: int, scan: Scan, data: list) -> None:
        assert (len(data) == 1)
        self.results.iat[((row + 1) * (col + 1)) - 1, int(scan) + self.FIRST_COL] = data[0]

    def __max_col_refs(self, col: int) -> None:
        pass

    def __max_row_refs(self, row: int) -> None:
        pass

    def __max_pixel_ref(self, row: int, col: int) -> None:
        df = self.results.iloc[((row + 1) * (col + 1)) - 1,
                               self.FIRST_COL:self.FIRST_COL + len(Scan) + 1]
        return max(df.values.tolist())

    def __create_empty_file(self):
        row = []
        for r in range(0, 56):
            row += [r] * 192
        col = [c for c in range(0, 192)] * 56
        empty = [-1] * 56 * 192

        data = {'row': row, 'col': col, 'center': empty,
                'right': empty, 'right_down': empty, 'down': empty, 'left_down': empty,
                'left': empty, 'left_up': empty, 'up': empty, 'right_up': empty}

        df = pd.DataFrame(data)
        df.to_csv(self.filename)

    def to_csv(self) -> None:
        self.results.to_csv(self.filename, index=False)
