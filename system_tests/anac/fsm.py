import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from transitions import Machine
from capture import Capture
from transitions.extensions.asyncio import AsyncMachine
from anac.results import Scan
import asyncio
import ctypes
import signal

NUM_OF_IMAGE_DONES = 1


class AngularAccuracyFsm(object):

    def __init__(self, loop, capture, board_controller, trx_board_align, lidar_controller, results):
        self.capture = capture
        self.board_controller = board_controller
        self.trx_board_align = trx_board_align
        self.lidar_controller = lidar_controller
        self.results = results

    async def start(self):
        print('starting angular accuracy fsm...')
        self.factory = await self.lidar_controller.factory()
        await self.lidar_controller.play()
        await self.board_controller.init()
        await self.do_test()

    async def do_test(self):
        print('starting the angular accuracy test...')
        x0, y0 = await self.board_controller.where()
        for col in range(0, 192):
            await self.scan_col(col, x0)
        await self.analyze()

    async def scan_col(self, col: int, x0: float) -> None:
        y = await self.trx_board_align.y0(col)
        await self.board_controller.goto(x0, y)
        scans = [Scan.CENTER]
        for scan in scans:
            # move the board to the next scan
            await self._capture()
            self._record(col, scan)

    async def _capture(self) -> None:
        self.capture.start(self.factory, NUM_OF_IMAGE_DONES)
        await asyncio.sleep((NUM_OF_IMAGE_DONES + 1) * 0.05)
        assert(self.capture.ignore == True)
        self.capture.stop()

    def _record(self, col: int, scan: Scan) -> None:
        refs = self.capture.reflectivity.col(col)
        self.results.set_refs(-1, col, scan, refs)

    async def analyze(self):
        print('analyzing angular accuracy results...')
        await self.lidar_controller.stop()
        await self.board_controller.stop()
        self.results.to_csv(index=False)
        self.close()

    def close(self):
        print('angular accuracy fsm committing suiside...')
        self.kill()

    async def listen_for_tests(self, num_of_iter, gap):
        print('black_box_test')
        for i in range(num_of_iter):
            print(f'going to sleep for {gap}...')
            await asyncio.sleep(gap)
        self.kill()

    def kill(self):
        print('commit suiside!')
        ucrtbase = ctypes.CDLL('ucrtbase')
        c_raise = ucrtbase['raise']
        c_raise(signal.SIGINT)

# todo:
# use transitions and use class AsyncModel https://github.com/pytransitions/transitions
# add callback to capture start and await
# why do i need the loop here?
