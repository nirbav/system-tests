import os
import sys
sys.path.append(os.path.realpath(os.path.join('..')))
from board import BoardController
from cnc2790 import CncOpenBuilds2790Grbl
from alignment import TrxBoardAlignment
from lidar import Lidar
from anac.fsm import AngularAccuracyFsm
from anac.results import AngularAccuracyResults
from reflectivity import Reflectivity
from capture import Capture
import os
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)
import argparse
import asyncio


parser = argparse.ArgumentParser()
parser.add_argument('--angular_accuracy', help='angular accurancy help')
args = parser.parse_args()

HOST, PORT = '127.0.0.1', 8888

if __name__ == '__main__':
    trx_mipi = 1
    y0_trx_board_align_filename = os.path.join('..', 'tests', 'tested_y0_mipi_1.csv')
    reflectivity = Reflectivity()
    capture = Capture(reflectivity)
    board_controller = BoardController(CncOpenBuilds2790Grbl())
    trx_board_align = TrxBoardAlignment(y0_trx_board_align_filename)
    lidar_controller = Lidar(trx_mipi)
    aaresults_filename = os.path.join('..', 'tests', 'tested_angular_accuracy_results.csv')
    results = AngularAccuracyResults(aaresults_filename)

    loop = asyncio.get_event_loop()
    fsm = AngularAccuracyFsm(loop, capture, board_controller,
                             trx_board_align, lidar_controller, results)
    loop.run_until_complete(loop.create_datagram_endpoint(lambda: capture, local_addr=(HOST, PORT)))
    loop.run_until_complete(fsm.start(),)
    loop.run_forever()
    print('stopped')

# todo:
# add com, port, trx_mipi and board axiss offsetes as args
# add gui for configuration
# use pattern
# add : and -> in entire code + code convention
