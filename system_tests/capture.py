from reflectivity import Reflectivity
import asyncio
import numpy as np

SKIP_1ST_PARTIAL_IMAGE = -1


class Capture(asyncio.DatagramProtocol):

    def __init__(self, reflectivity):
        super().__init__()
        self.ignore = True
        self.num_of_image_dones = 1
        self.image_done_counter = 0
        self.reflectivity = reflectivity

    def connection_made(self, transport) -> 'Used by asyncio':
        self.transport = transport

    def start(self, factory: hex, num_of_image_dones=1) -> None:
        self.factory = factory
        self.num_of_image_dones = num_of_image_dones
        self.image_done_counter = SKIP_1ST_PARTIAL_IMAGE
        self.reflectivity.clear()
        self.ignore = False

    def stop(self) -> None:
        self.ignore = True

    def done(self) -> None:
        return self.image_done_counter >= self.num_of_image_dones

    def paused(self) -> None:
        return self.ignore

    def datagram_received(self, data, addr) -> 'Main entrypoint for processing message':
        # print(f"datagram_received: {len(data)}, ignore {self.ignore}")
        # print(f"datagram_received: {data}")

        if self.ignore == False:
            hdr = np.frombuffer(data, dtype=np.uint8, count=8)
            if hdr[0] == self.factory:
                if self.image_done_counter != SKIP_1ST_PARTIAL_IMAGE:
                    payload = np.frombuffer(data, dtype=np.uint8, count=1024, offset=8)
                    datapoints = payload.reshape(128, 8)

                    row = datapoints[:, 0]
                    col = datapoints[:, 1]
                    tof = datapoints[:, 4]
                    ref = datapoints[:, 7]
                    ref = np.where(tof == 255, 0, ref)
                    i = np.ogrid[:128]
                    self.reflectivity.data[row[i], col[i]] += ref[i]

                if hdr[1] & 0x80:
                    self.image_done_counter += 1

                if self.image_done_counter >= self.num_of_image_dones:
                    self.ignore = True
