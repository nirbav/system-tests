import time
import serial
import asyncio


class CncOpenBuilds2790Grbl:

    MS_100_SLEEP = 0.1
    SEC_ONE_SLEEP = 1.0
    ABSOLUTE_VALUES = 'G90'
    INCCREMENTAL_VALUES = 'G91'
    MM_COORDINATES = 'G21'

    def __init__(self, baudrate=115200, port='COM4'):
        self.ser = serial.Serial()
        self.ser.baudrate = baudrate
        self.ser.port = port
        self.ser.timeout = self.MS_100_SLEEP
        self.ser.timeout = None
        self.jog = mm_jog = 5
        self.feed_rate = mm_per_minute_feed_rate = 4997

    async def init(self) -> None:
        await self.open_serial()
        await asyncio.sleep(self.MS_100_SLEEP)
        if self.ser.isOpen():
            await self.cancel_alarm()
            await self.home()

    async def stop(self) -> None:
        await self.close_serial()

    async def open_serial(self) -> None:
        print('open serial port baudrate ', self.ser.baudrate, ' port ', self.ser.port)
        try:
            if not self.ser.isOpen():
                await self.ser.open()
        except AssertionError as error:
            print('failed to open serial connection, ', error)

    async def close_serial(self) -> None:
        print('close serial port')
        await self.ser.close()

    def write_and_log(function):
        def wrapper_write(self, cmd: str):
            print("write: {}".format(cmd))
            function(self, cmd)
        return wrapper_write

    @write_and_log
    def write(self, cmd: str):
        if self.ser.isOpen():
            self.ser.flushInput()
            self.ser.write('{}\n'.format(cmd).encode())

    async def write_and_wait(self, cmd: str) -> str:
        print("__write_and_wait: {}".format(cmd))
        resp = '\r\n'
        if self.ser.isOpen():
            self.ser.timeout = None
            await self.ser.flushInput()
            await self.ser.write('{}\n'.format(cmd).encode())
            resp = self.ser.readline().decode()
            self.__print_response(resp)
        return resp

    def __print_response(self, response):
        length = len(response)
        if length >= 5:
            if response[0:5] == 'error':
                print('got {} error code'.encode(resp.split(':')[1]))
                return
        if length >= 2:
            if response[0:2] == 'ok':
                print('OK, the operation may still be in progress')
                return
        print(response)

    async def cancel_alarm(self) -> None:
        await self.write_and_wait('$')

    async def home(self) -> None:
        await self.write_and_wait('$H')

    async def up(self) -> None:
        cmd = '$J={}{}Y{}F{}'.format(
            self.INCCREMENTAL_VALUES, self.MM_COORDINATES, self.jog, self.feed_rate)
        await self.write_and_wait(cmd)

    async def down(self) -> None:
        cmd = '$J={}{}Y-{}F{}'.format(self.INCCREMENTAL_VALUES,
                                      self.MM_COORDINATES, self.jog, self.feed_rate)
        await self.write_and_wait(cmd)

    async def right(self) -> None:
        cmd = '$J={}{}X{}F{}'.format(
            self.INCCREMENTAL_VALUES, self.MM_COORDINATES, self.jog, self.feed_rate)
        await self.write_and_wait(cmd)

    async def left(self) -> None:
        cmd = '$J={}{}X-{}F{}'.format(self.INCCREMENTAL_VALUES,
                                      self.MM_COORDINATES, self.jog, self.feed_rate)
        await self.write_and_wait(cmd)

    async def check_for_completion(self):
        return True

    async def poll_for_completion(self):
        complete = False
        while True:
            await asyncio.sleep(self.MS_100_SLEEP)
            complete = await self.check_for_completion()
            if complete == True:
                break

    def wait_according_moving_rate(self, time_in_ms):
        pass

    async def goto(self, x: float = 0, y: float = 0) -> None:
        valid = True
        if x > 0 and y == 0:
            cmd = '$J={}X{}Y{}'.format(self.ABSOLUTE_VALUES, x, y)
        elif X > 0 and Y == 0:
            cmd = '$J={}X{}'.format(self.ABSOLUTE_VALUES, x)
        elif X == 0 and Y > 0:
            cmd = '$J={}X{}'.format(self.ABSOLUTE_VALUES, y)
        else:
            valid = False

        if valid == True:
            await self.write_and_wait(cmd)

    async def where(self):
        resp = await self.write_and_wait('?')
        resp = resp.decode()
        self.__print_response(resp)
        resp = resp.split(':')[1]
        x, y = resp.split(',')[0], resp.split(',')[1]
        return [float(x), float(y)]


# todo:
# to start the algorithm after calculation of the pixel shift from 0.0
# calculate the time according the the rate and consider await accordinglt instead of blocking write
# find a way to mock private functions in the test and add __ before those methods
